﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompassbyOleg
{
    public class PocketData
    {
        public PocketData(double pitch, double roll, double heading)
        {
            Pitch = pitch;
            Roll = roll;
            Heading = heading;
        }
        public double Pitch { get; set; }
        public double Roll { get; set; }
        public double Heading { get; set; }
    }
}
