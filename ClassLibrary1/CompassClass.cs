﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics;
using System;

namespace CompassbyOleg
{
    public enum Speed:int
    {
        Speed_2400=2400,
        Speed_4800 = 4800,
        Speed_9600 = 9600,
        Speed_19200 = 19200,
        Speed_38400 = 38400,
        Speed_57600 = 57600,
        Speed_115200 = 115200,
    }

    public class CompassClass
    {
        private SerialPort port;
        public event EventHandler<byte[]> OnWriteBytes;
        public event EventHandler<byte[]> OnReadBytes;
        public event EventHandler<string> OnError;
        public event EventHandler<bool> IsPortOpened;
        public event EventHandler<PocketData> PocketReceived;
        Thread readlistThread;
        public CompassClass(int comnumber)
        {
            try
            {
                port = new SerialPort("COM" + comnumber.ToString(), 9600, Parity.None, 8, StopBits.One);
            } 
            catch (Exception ex)
            {
                OnError?.Invoke(this, "Invalid input data!");
            }
        }
        public CompassClass(string comnumber)
        {
            try
            {
                port = new SerialPort(comnumber.Replace(" ", ""), 9600, Parity.None, 8, StopBits.One);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, "Invalid input data!");
            }
        }
        public CompassClass(int comnumber, Speed speed)
        {
            try
            {
                port = new SerialPort("COM" + comnumber.ToString(), (int)speed, Parity.None, 8, StopBits.One);
            }
            catch(Exception ex)
            {
                OnError?.Invoke(this, "Invalid input data!");
            }
        }
        public CompassClass(string comnumber, Speed speed)
        {
            try
            {
                port = new SerialPort(comnumber.Replace(" ", ""), (int)speed, Parity.None, 8, StopBits.One);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, "Invalid input data!");
            }
        }
        bool isOpened = false;
        public bool Open()
        {
            try
            {
                if (isOpened == false)
                {
                    port.DataReceived += port_DataReceived;
                    port.Open();
                    isOpened = true;
                    Startquery();
                    IsPortOpened?.Invoke(this, true);

                }
                return true;
            }
            catch(Exception ex)
            {
                OnError?.Invoke(this, "Port is unaccessible!");
                 return false;
            }
        }
        //bool _continue = false;
        public void Go()
        {
          //_continue = true;
          //отрисовка 
        }
        public void Stop() 
        {
            //_continue = false;
            //прекратить отрисовку
        }
        public bool Close()
        {
            try                                                                                      
            {
                isOpened = false;
                if (port != null && port.IsOpen)
                {
                    port.DataReceived -= port_DataReceived;
                    port.Close();
                    port = null;
                }
                IsPortOpened?.Invoke(this, false);
                if (readlistThread != null)
                {
                    readlistThread.Abort();
                    readlistThread.Join(500);
                    readlistThread = null;
                }
                return true;
            }
            catch (Exception ex)
            {
              OnError?.Invoke(this, "Closin operation failed!");
              return false;
            }
        }
        int datalength = 0;
        byte[] datacurrent = new byte[350];
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                while (datalength <= 13 )
                {
                    if (isOpened == true)
                    {
                        int bytes2read = port.BytesToRead;
                        port.Read(datacurrent, datalength, bytes2read);
                        datalength = datalength + bytes2read;
                    }
                    else { break; }
                }
                OnReadBytes?.Invoke(this, datacurrent);
                for (int j = 0; j < datalength; j++)
                {
                    if (datacurrent[j] == 104 && datacurrent[j + 3] == 132)
                    {
                        byte[] data2parse = new byte[9];
                        int a = 0;
                        int offset=datalength;
                        while (datalength <= j + 12)
                        {
                            if (isOpened==true)
                            {
                                int bytes2read = port.BytesToRead;
                                port.Read(datacurrent, offset, bytes2read);
                                offset = offset + bytes2read;
                            }
                            else { break; }
                        }
                        for (int k = j + 4; k <= j + 12; k++)
                        {
                            data2parse[a] = datacurrent[k];
                            a++;
                        }
                        ParsePocket(data2parse);
                        datalength = 0;
                        break;
                    }
                }
                datacurrent = new byte[350];
            }
            catch(Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                OnError(this, $"Exception: {ex}");

            }
        }
        List<PocketData> list = new List<PocketData>();
             
        private void ParsePocket(byte[] data) 
        {
            var pitch = ConvertIncomeData2Double(data, 0);
            var roll = ConvertIncomeData2Double(data, 3);
            var heading = ConvertIncomeData2Double(data, 6);
            PocketData pocket = new PocketData(pitch,roll,heading);
            PocketReceived?.Invoke(this, pocket);
            list.Add(pocket);
            AdjustArray(list);
            if (list.Count() >= 20) { list = new List<PocketData>(); }
        }
        private async void Startquery()
        {
            while (isOpened)
            {
                byte[] data = { 104, 04, 00, 04, 08 };
                port.Write(data, 0, data.Length);
                OnWriteBytes?.Invoke(this, data);
                await Task.Delay(150);
            }
        }
        double curheading = -1;
        double curroll = -1;
        double curpitch = -1;
        double[] rollarray= { };
        double[] pitcharray = { };
        double[] headingarray= { };
        public void AdjustArray(List<PocketData> array)
        {
            int count = array.Count();
            rollarray = new double[count];
            pitcharray = new double[count];
            headingarray = new double[count];
            for(int i = 0; i<count;i++)
            {
                rollarray[i] = array[i].Roll;
                pitcharray[i] = array[i].Pitch;
                headingarray[i] = array[i].Heading;
            }
            rollarray = DelFluct(rollarray);
            pitcharray = DelFluct(pitcharray);
            headingarray = DelFluct(headingarray);

            for(int i=0; i<count;i++)
            {
                array[i].Roll = rollarray[i];
                array[i].Pitch = pitcharray[i];
                array[i].Heading = headingarray[i];
            }
            curpitch = array[array.Count - 1].Pitch;
            curroll = array[array.Count - 1].Roll;
            curheading = array[array.Count - 1].Heading;
        }
        private double[] DelFluct(double[] array)
        {
            double medianvalue = 0;
            int count = array.Length;
            if (count % 2 == 0)
            {
                double middleElement1 = array[(count / 2) - 1];
                double middleElement2 = array[(count / 2)];
                medianvalue = (middleElement1 + middleElement2) / 2;
            }
            else
            {
                medianvalue = array[count / 2];
            }
            for (int i   = 0; i < array.Length; i++)
            {
                double diff = Math.Abs(medianvalue - array[i]);
                if (diff >= 5) {   array[i] = medianvalue; }//array = array.Where(val => val != i).ToArray(); }
            }
            return array;
        }
        public NumberFormatInfo format = new NumberFormatInfo();
        private double ConvertIncomeData2Double(byte[] data, int startindex)
        {
            format.NegativeSign = "-";
            format.NumberDecimalSeparator = ".";
            string strvalue = (data[startindex].ToString("X2")[0].Equals("1") ? "-" : "+") + data[startindex].ToString("X2")[1] + data[startindex + 1].ToString("X") + "." + data[startindex + 2].ToString("X");
            var  doublevalue= Double.Parse(strvalue, format);
            return doublevalue;
        }
        public double GetPitch()
        {
            if (list.Count() > 0) { return list[list.Count - 1].Pitch; }
            else return curpitch;
        }
        public double GetRoll()
        {
            if (list.Count() > 0) { return list[list.Count - 1].Roll; }
            else return curroll;
        }
        public double GetHeading()
        {
            if (list.Count() > 0) { return list[list.Count - 1].Heading; }
            else return curheading;
        }
    }
}
