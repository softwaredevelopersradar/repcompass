﻿using CompassbyOleg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CompassClass compass;
        public MainWindow()
        {
            InitializeComponent();
           
            //compass.OnReadBytes += Writetobox;
            connectEl.Fill = new SolidColorBrush(Colors.Red);
        }

        public void Writetobox(object sender, byte[] e)
        {
            string mess = "";
            foreach (var a in e)
                mess += $"{a.ToString()}";
            Dispatcher.Invoke(() => { Box.Items.Add(mess); });
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (compass != null)
            {
                compass.Close();
            }
            if (String.IsNullOrWhiteSpace(textBox.Text)) { compass = new CompassClass(3, Speed.Speed_9600); }
                else
                {
                    try
                    {
                        compass = new CompassClass(int.Parse(textBox.Text));
                    }
                    catch (Exception ex)
                    {
                        compass = new CompassClass(textBox.Text);
                    }
                }
                bool connection = compass.Open();
                if (connection == true) { connectEl.Fill = new SolidColorBrush(Colors.PaleGreen); }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            bool disconnection=false;
             
            if (compass != null) {disconnection = compass.Close(); }
            if (disconnection == true) { connectEl.Fill = new SolidColorBrush(Colors.Red); }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            compass.Go();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            compass.Stop();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            string pitch = compass.GetPitch().ToString() + "\u00B0";
            Box.Items.Add(pitch);
            Box.SelectedIndex = Box.Items.Count - 1;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            string roll = compass.GetRoll().ToString() +"\u00B0";
            Box.Items.Add(roll);
            Box.SelectedIndex = Box.Items.Count - 1;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            string heading = compass.GetHeading().ToString() + "\u00B0";
            Box.Items.Add(heading);
            Box.SelectedIndex = Box.Items.Count - 1;
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }
    }
}
